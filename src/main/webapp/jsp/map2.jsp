<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        body, html{width: 100%;height: 100%;margin:0;font-family:"微软雅黑";}
        #allmap{height:500px;width:100%;}
        #r-result{width:100%; font-size:14px;}
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=P84lgPNpK7lGdNuiG87avraYKgO1oeLh"></script>
    <script type="text/javascript" src="/wechat/js/jweixin-1.2.0.js"></script>
    <title>城市名定位</title>
</head>
<body>
<div id="allmap" style="height: 90%"></div>
<div id="r-result" style="height: 10%">

    <input type="button" value="出发"  onclick="aa()"/>
    <input type="button" value="结束"  onclick="bb()"/>
</div>

</body>
</html>
<script type="text/javascript">
    console.log("this is 11111111");
    // 百度地图API功能

    var latitude,longitude;
    // 用经纬度设置地图中心点
    var map ;
    var t;
    var tempPoint;

    function getLocation(){
        wx.getLocation({
            type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
            success: function (res) {
                latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
                longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
                var speed = res.speed; // 速度，以米/每秒计
                var accuracy = res.accuracy; // 位置精度

                console.log("this is ready latitude"+latitude);
                console.log("this is ready longitude"+longitude);
                if(typeof(map)=="undefined"){
                    map = new BMap.Map("allmap");
                }
                var new_point = new BMap.Point(longitude,latitude);
                if(typeof(tempPoint) == "undefined"){
                    tempPoint = new BMap.Point(longitude,latitude);
                }
                var polyline = new BMap.Polyline([
                    tempPoint,
                    new_point
                ], {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});   //创建折线
                map.addOverlay(polyline);   //增加折线
                tempPoint = new_point;
                deleteLastPoint();
                var convertor = new BMap.Convertor();
                var pointArr = [];
                pointArr.push(new_point);
                convertor.translate(pointArr, 1, 5, translateCallback)

//                map.centerAndZoom(new BMap.Point(longitude,latitude),15);
//                map.enableScrollWheelZoom(true);
//
//                map.clearOverlays();
//
//                var marker = new BMap.Marker(new_point);  // 创建标注
//                map.addOverlay(marker);              // 将标注添加到地图中
//                map.panTo(new_point);
                console.log("this is get location=========");

            },cancel: function (res){

            }
        });
        if(typeof(map)!="undefined"){
            t=setTimeout("getLocation()",5000);
        }

    }


    wx.config({
        debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: 'wx1afe909d0966e68d', // 必填，公众号的唯一标识
        timestamp:'${timeStamp}', // 必填，生成签名的时间戳
        nonceStr: '${nonceStr}', // 必填，生成签名的随机串
        signature:'${signature}',// 必填，签名
        jsApiList: [
            'openLocation',
            'getLocation'
        ] // 必填，需要使用的JS接口列表，所有JS接口列表见官方文档附录2
    });
    wx.ready(function(){
        console.log("this is ready");
        getLocation();

        map.enableScrollWheelZoom(true);
        map.addTileLayer(new BMap.PanoramaCoverageLayer());
        var stCtrl = new BMap.PanoramaControl(); //构造全景控件
        stCtrl.setOffset(new BMap.Size(20, 20));
        map.addControl(stCtrl);//添加全景控件

        console.log("this is 3333333333");

    });
    wx.error(function(res){
        console.log("this is error");
    });

    function aa(){
        t=setTimeout("getLocation()",5000);
    }

    function bb(){
        clearTimeout(t)
    }

    function addMarker(point,label){
        var marker = new BMap.Marker(point);
        map.addOverlay(marker);
        marker.setLabel(label);
    }

    function deleteLastPoint(){
        var allOverlay = map.getOverlays();
        for (var i = 0; i < allOverlay.length -1; i++){
            if(allOverlay[i].getLabel().content == "now"){
                map.removeOverlay(allOverlay[i]);
                return false;
            }
        }
    }

    translateCallback = function (data){
        //alert(data);
        if(data.status === 0) {
            var label = new BMap.Label("now",{offset:new BMap.Size(20,-10)});
            addMarker(data.points[0],label);
            map.centerAndZoom(data.points[0],15);
            map.addOverlay(marker);
            //var label = new BMap.Label("转换后的百度坐标（正确）",{offset:new BMap.Size(20,-10)});
            //marker.setLabel(label); //添加百度label
            map.setCenter(data.points[0]);
            //map.centerAndZoom(new BMap.Point(longitude,latitude),15);
        }
    }



</script>

