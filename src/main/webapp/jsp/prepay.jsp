<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
<head>
    <title>生成订单</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="/wechat/js/jquery.min.js"></script>
    <script type="text/javascript" src="/wechat/js/jweixin-1.2.0.js"></script>
    <style>
        *{
            margin:0;
            padding:0;
        }
        ul,ol{
            list-style:none;
        }
        body{
            font-family: "Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        }
        .hidden{
            display:none;
        }
        .new-btn-login-sp{
            padding: 1px;
            display: inline-block;
            width: 75%;
        }
        .new-btn-login {
            background-color: #09bb07;
            color: #FFFFFF;
            font-weight: bold;
            border: none;
            width: 100%;
            height: 30px;
            border-radius: 5px;
            font-size: 16px;
        }
        #main{
            width:100%;
            margin:0 auto;
            font-size:14px;
        }
        .red-star{
            color:#f00;
            width:10px;
            display:inline-block;
        }
        .null-star{
            color:#fff;
        }
        .content{
            margin-top:5px;
        }
        .content dt{
            width:100px;
            display:inline-block;
            float: left;
            margin-left: 20px;
            color: #666;
            font-size: 13px;
            margin-top: 8px;
        }
        .content dd{
            margin-left:120px;
            margin-bottom:5px;
        }
        .content dd input {
            width: 85%;
            height: 28px;
            border: 0;
            -webkit-border-radius: 0;
            -webkit-appearance: none;
        }
        #foot{
            margin-top:10px;
            position: absolute;
            bottom: 15px;
            width: 100%;
        }
        .foot-ul{
            width: 100%;
        }
        .foot-ul li {
            width: 100%;
            text-align:center;
            color: #666;
        }
        .note-help {
            color: #999999;
            font-size: 12px;
            line-height: 130%;
            margin-top: 5px;
            width: 100%;
            display: block;
        }
        #btn-dd{
            margin: 20px;
            text-align: center;
        }
        .foot-ul{
            width: 100%;
        }
        .one_line{
            display: block;
            height: 1px;
            border: 0;
            border-top: 1px solid #eeeeee;
            width: 100%;
            margin-left: 20px;
        }
        .am-header {
            display: -webkit-box;
            display: -ms-flexbox;
            display: box;
            width: 100%;
            position: relative;
            padding: 7px 0;
            -webkit-box-sizing: border-box;
            -ms-box-sizing: border-box;
            box-sizing: border-box;
            background: #1D222D;
            height: 50px;
            text-align: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            box-pack: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            box-align: center;
        }
        .am-header h1 {
            -webkit-box-flex: 1;
            -ms-flex: 1;
            box-flex: 1;
            line-height: 18px;
            text-align: center;
            font-size: 18px;
            font-weight: 300;
            color: #fff;
        }
    </style>
</head>
<body text=#000000 bgColor="#ffffff" leftMargin=0 topMargin=4>
<header class="am-header">
    <h1>微信支付订单</h1>
</header>

<div id="main">
    <form name=alipayment action='' method=post >
        <div id="body" style="clear:left">
            <dl class="content">
                <dt>商户订单号：</dt>
                <dd>
                    <input id="WIDout_trade_no" name="WIDout_trade_no" disabled="disabled" />
                </dd>
                <hr class="one_line">

                <dt>付款金额：</dt>
                <dd>
                    <input id="WIDtotal_amount1" name="WIDtotal_amount1"   disabled="disabled"/>

                </dd>
                <hr class="one_line"/>
                <dt>商品描述：</dt>
                <dd>
                    <input id="WIDbody1" name="WIDbody1"    disabled="disabled"/>

                </dd>
                <hr class="one_line">
                <dt></dt>
                <div class='pay_buttom'>
                    <a href="#" style="background: #06C; color: #fff;" onclick="dopay();">确认支付</a>
                </div>
            </dl>
        </div>
    </form>
    <div id="foot">
        <ul class="foot-ul">
            <li>
                pay.weixin.qq.com
            </li>
        </ul>
    </div>
</div>
</body>
<script language="javascript">
    function GetDateNow() {
        var vNow = new Date();
        var sNow = "";
        sNow += String(vNow.getFullYear());
        sNow += String(vNow.getMonth() + 1);
        sNow += String(vNow.getDate());
        sNow += String(vNow.getHours());
        sNow += String(vNow.getMinutes());
        sNow += String(vNow.getSeconds());
        sNow += String(vNow.getMilliseconds());
        document.getElementById("WIDout_trade_no").value =  sNow;
        //document.getElementById("WIDout_trade_no1").value =  sNow;

        document.getElementById("WIDtotal_amount1").value = "0.01";
         document.getElementById("WIDbody1").value = "购买测试商品0.01元";
    }
    GetDateNow();

    function dopay() {

        //获取当前浏览器url全路径,如: http://zhoubang85.com/chooseWXPay.jsp
        var clientUrl = window.location.href;
        //请求后台，获取jssdk支付所需的参数
        $.ajax({
            type : 'post',
            url : './pay',
            dataType : 'json',
            data : {
                "commodityName" :"aaaaa", //商品名称
                "totalPrice" : 1, //支付的总金额
                "clientUrl" : clientUrl
                //当前页面所在的浏览器URL全路径,由于该支付为jssdk支付，所以需要url地址.参与后台sign签名
            },
            cache : false,
            error : function() {
                alert("系统错误，请稍后重试");
                return false;
            },
            success : function(data) {
                //微信支付功能只有微信客户端版本大于等于5.0的才能调用
                if (parseInt(data[0].agent) < 5) {
                    alert("您的微信版本低于5.0无法使用微信支付");
                    return;
                }
                //JSSDK支付所需的配置参数，首先会检查signature是否合法。
                wx.config({
                    debug : true, //开启debug模式，测试的时候会有alert提示
                    appId : data[0].appid, //公众平台中-开发者中心-appid
                    timestamp : data[0].config_timestamp, //时间戳
                    nonceStr : data[0].config_nonceStr, //随机字符串,不长于32位
                    signature : data[0].config_sign, //这里的signature是后台使用SHA1签名算法得出，不是MD5，与下面的wx.chooseWXPay中的paySign不同，下面的paySign是后台使用MD5加密得出
                    jsApiList : [ //指定哪些JS接口有权限访问
                        'chooseWXPay' ]
                });

                /*
                    1、wx.chooseWXPay支付的回调：
                        a、success ： 支付成功后，会调用该回调方法
                        b、cancel ： 用户手动取消支付，关闭支付控件，会调用该回调方法
                        c、fail ： 支付失败，会调用该回调方法
                        d、complete ： 无论支付成功还是失败，都会调用该方法
                    2、通过1中的描述，发起支付后的回调可以有2种方式进行处理：
                        a、分别加入success、cancel、fail这3个回调函数，分别处理不同的支付状态，如：下方注释的代码片断。
                        b、只写一个complete回调函数，然后判断回调函数中的res对象的errMsg属性值，处理不同的支付状态.
                 */
                //上方的config检测通过后，会执行ready方法
                wx.ready(function() {
                    wx.chooseWXPay({
                        timestamp : data[0].config_timestamp, // 支付签名时间戳。前端js中指定的timestamp字段均为小写。后台生成签名使用的timeStamp字段需大写其中的S字符，即：timeStamp
                        nonceStr : data[0].config_nonceStr, // 支付签名随机串，不长于 32 位
                        package : data[0].packageValue, // 统一支付接口返回的prepay_id参数值，格式：prepay_id=***）
                        signType : "MD5", // 签名方式MD5，不是SHA1，后台使用MD5加密，与上面的wx.config中的signature不同。
                        paySign : data[0].paySign, // 后台生成的支付签名串
                        /*
                        success: function (res) {
                            alert("支付成功！");
                            window.location.href = data[0].sendUrl; //成功后，跳转到自定义的支付成功的页面
                        },
                        cancel: function (res) {
                            alert("您已手动取消该订单支付。");
                        },
                        fail : function(res){
                            alert("订单支付失败。");
                        },
                         */
                        //该complete回调函数，相当于try{}catch(){}异常捕捉中的finally，无论支付成功与否，都会执行complete回调函数。即使wx.error执行了，也会执行该回调函数.
                        complete : function(res) {
                            alert("complete回调方法返回值" + res.errMsg);

                            /*注意：res对象的errMsg属性名称，是没有下划线的，与WeixinJSBridge支付里面的err_msg是不一样的。而且，值也是不同的。*/
                            if (res.errMsg == "chooseWXPay:ok") {
                                alert("支付成功");
                                window.location.href = data[0].sendUrl;
                            } else if (res.errMsg == "chooseWXPay:cancel") {
                                alert("你手动取消支付");
                            } else if (res.errMsg == "chooseWXPay:fail") {
                                alert("支付失败");
                            } else if (res.errMsg == "config:invalid signature") {
                                alert("支付签名验证错误，请检查签名正确与否 or 支付授权目录正确与否等");
                            }
                        }
                    });
                });
                wx.error(function(res) {
                    if (res.errMsg == "config:invalid url domain") {
                        alert("微信支付(测试)授权目录设置有误");
                    } else {
                        alert("检测出问题:" + res.errMsg);
                    }
                });
            }
        });
    }
</script>
</html>
