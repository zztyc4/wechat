package com.dendi.config;

import com.github.wxpay.sdk.WXPayConfig;
import java.io.*;

public class MyConfig implements WXPayConfig{

    private byte[] certData;

    public MyConfig() throws Exception {
//       String t=Thread.currentThread().getContextClassLoader().getResource("").getPath();
//
//        String path=t.replaceFirst("/","")+"/apiclient_cert.p12";

        String certPath = "/icymi/apiclient_cert.p12";
        File file = new File(certPath);
        InputStream certStream =new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }

    public String getAppID() {
        return "wx1afe909d0966e68d";
    }

    public String getMchID() {
        return "1487082672";
    }

    public String getKey() {
        return "wanganyimihongyangcuiwenteng1234";
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    public int getHttpConnectTimeoutMs() {
        return 8000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }
}