package com.dendi.controller;

import com.dendi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/")
	public String index( ModelMap model) {
		System.out.println(userService.getUserById("52"));
		model.addAttribute("noncestr","bbbbbbbbbbbbbbbbbbb");
		return "index";
	}

}
