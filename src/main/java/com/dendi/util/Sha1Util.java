package com.dendi.util;


import java.security.MessageDigest;
import java.util.*;

/*
'============================================================================
'api说明：
'createSHA1Sign创建签名SHA1
'getSha1()Sha1签名
'============================================================================
'*/
public class Sha1Util {
	public static String getNonceStr() {
		Random random = new Random();
		return MD5Util.MD5Encode(String.valueOf(random.nextInt(10000)), "UTF-8");
	}
	public static String getTimeStamp() {
		return String.valueOf(System.currentTimeMillis() / 1000);
	}
	
   //创建签名SHA1
	public static String createSHA1Sign(SortedMap<String, String> signParams) throws Exception {
		StringBuffer sb = new StringBuffer();
		Set es = signParams.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			sb.append(k + "=" + v + "&");
			//要采用URLENCODER的原始值！
		}
		String params = sb.substring(0, sb.lastIndexOf("&"));
		return getSha1(params);
	}
	//Sha1签名
	public static String getSha1(String str) {
		if (str == null || str.length() == 0) {
			return null;
		}
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };

		try {
			MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
			mdTemp.update(str.getBytes("UTF-8"));
			byte[] md = mdTemp.digest();
			int j = md.length;
			char buf[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
				buf[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(buf);
		} catch (Exception e) {
			return null;
		}
	}
	public static void main(String[] args) throws Exception {
		SortedMap<String, String> finalpackage = new TreeMap<String, String>();
		String timestamp = Sha1Util.getTimeStamp();
		//finalpackage.put("appId", WeiChatConfig.appid);
		finalpackage.put("timestamp", "1496951550");
		finalpackage.put("noncestr", "d130ca139ad84f479ed49406b220862b");
		finalpackage.put("jsapi_ticket", "kgt8ON7yVITDhtdwci0qeXZ5dXwKA2vWKFzfgQLOoSepnY2WdtS2v8Ccr1DcVfOAVElmBE14sFHDJyBSS_B_gQ");
		finalpackage.put("url", "http://www.zizai.pro/zz-web/weiChat/toPay");
		//finalpackage.put("package", packages);
		//finalpackage.put("signType", WeiChatConfig.signType);
		String signature =Sha1Util.createSHA1Sign(finalpackage);
		System.out.println("signature=="+signature);
		System.out.println(getSha1("jsapi_ticket=kgt8ON7yVITDhtdwci0qeXZ5dXwKA2vWKFzfgQLOoSepnY2WdtS2v8Ccr1DcVfOAVElmBE14sFHDJyBSS_B_gQ&noncestr=d130ca139ad84f479ed49406b220862b&timestamp=1496951550&url=http://www.zizai.pro/zz-web/weiChat/toPay"));
	}
}
