package com.dendi.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import com.dendi.util.WeiChatConfig;


public class MenuUtil {



    /**
     * @throws UnsupportedEncodingException
     * 创建Menu
     * @Title: createMenu
     * @Description: 创建Menu
     * @param @return
     * @param @throws IOException 设定文件
     * @return int 返回类型
     * @throws
     */
    public static String createMenu() throws UnsupportedEncodingException {

        String url1="https://open.weixin.qq.com/connect/oauth2/authorize?appid="+ WeiChatConfig.appid+"&redirect_uri=";
        //String url2="&redirect_uri=";
        String url2="&response_type=code&scope=snsapi_base&state=123#wechat_redirect";

        String v11str="http://www.icymi.cn/wechat/getLocation";
        v11str=URLEncoder.encode(v11str, "utf-8");

        String v12str="http://www.icymi.cn/wechat/getLocation1";
        v12str=URLEncoder.encode(v12str, "utf-8");

        String v13str="http://www.icymi.cn/wechat/getLocation2";
        v13str=URLEncoder.encode(v13str, "utf-8");

        String vurlPay="http://www.icymi.cn/wechat/toPay";
        vurlPay=URLEncoder.encode(vurlPay, "utf-8");

        String menu ="{\"button\":[{\"name\":\"出发\",\"sub_button\":[{\"type\":\"view\",\"name\":\"GO\",\"url\":\""+url1+""+v11str+""+url2+"\"},{\"type\":\"view\",\"name\":\"Pay\",\"url\":\""+url1+""+vurlPay+""+url2+"\"} ]} ,{\"name\":\"出发1\",\"sub_button\":[{\"type\":\"view\",\"name\":\"GO\",\"url\":\""+url1+""+v12str+""+url2+"\"} ]},{\"name\":\"出发2\",\"sub_button\":[{\"type\":\"view\",\"name\":\"GO\",\"url\":\""+url1+""+v13str+""+url2+"\"} ]}]}";
        //此处改为自己想要的结构体，替换即可
        String access_token= "7FdbOi4vXqAWsxcxDDTqtL-3gPqk7KEEIZikkWj8AA57SD0hi_msr1rm-7ZxQ1Jsb97gsH6fGIPLRjj2_MkjZsARG1VVf2dcYlwfMcOCaft7aP71CcJEc61MZIFmyTfiZZFbAFAPPV";
        System.out.println("access_token=="+access_token);
        String action = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+access_token;
        try {
            URL url = new URL(action);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            http.setRequestMethod("POST");
            http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            http.setDoOutput(true);
            http.setDoInput(true);
            System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
            System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
            http.connect();
            OutputStream os= http.getOutputStream();
            os.write(menu.getBytes("UTF-8"));//传入参数
            os.flush();
            os.close();

            InputStream is =http.getInputStream();
            int size =is.available();
            byte[] jsonBytes =new byte[size];
            is.read(jsonBytes);
            String message=new String(jsonBytes,"UTF-8");
            return "返回信息"+message;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "createMenu 失败";
    }
    /**
     * 删除当前Menu
     * @Title: deleteMenu
     * @Description: 删除当前Menu
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     */
    public static String deleteMenu()
    {
        String access_token= "";
        String action = "https://api.weixin.qq.com/cgi-bin/menu/delete? access_token="+access_token;
        try {
            URL url = new URL(action);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            http.setRequestMethod("GET");
            http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            http.setDoOutput(true);
            http.setDoInput(true);
            System.setProperty("sun.net.client.defaultConnectTimeout", "30000");//连接超时30秒
            System.setProperty("sun.net.client.defaultReadTimeout", "30000"); //读取超时30秒
            http.connect();
            OutputStream os= http.getOutputStream();
            os.flush();
            os.close();

            InputStream is =http.getInputStream();
            int size =is.available();
            byte[] jsonBytes =new byte[size];
            is.read(jsonBytes);
            String message=new String(jsonBytes,"UTF-8");
            return "deleteMenu返回信息:"+message;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "deleteMenu 失败";
    }
    public static void main(String[] args) throws IOException {

        //System.out.println(deleteMenu());
        System.out.println(createMenu());
    }
}
