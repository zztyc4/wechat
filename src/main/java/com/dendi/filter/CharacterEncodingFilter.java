package com.dendi.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jiyu on 2017/6/4.
 */
//@WebFilter(filterName = "CharacterEncodingFilter",urlPatterns="/*")
public class CharacterEncodingFilter implements Filter {
    private String defaultCharset = "UTF-8";
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String charset  = defaultCharset;

        request.setCharacterEncoding(charset);
        response.setCharacterEncoding(charset);
        response.setContentType("text/html;charset="+charset);
        chain.doFilter(req, response);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}

