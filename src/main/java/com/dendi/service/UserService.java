package com.dendi.service;

import com.dendi.entity.User;

import java.util.List;

public interface UserService {

	User getUserById(String id);

}
