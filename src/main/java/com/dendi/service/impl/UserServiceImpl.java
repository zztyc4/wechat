package com.dendi.service.impl;

import com.dendi.dao.UserDAO;
import com.dendi.entity.User;
import com.dendi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDAO userDao;


	public User getUserById(String id) {
		return userDao.getUserById(id);
	}
}
