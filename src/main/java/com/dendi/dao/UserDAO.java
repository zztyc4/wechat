package com.dendi.dao;

import com.dendi.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserDAO {

	@Select("select * from user where id = #{id}")
	User getUserById(String id);
}
