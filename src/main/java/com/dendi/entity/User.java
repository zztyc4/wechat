package com.dendi.entity;

public class User {
	private Integer id;
	private String name;
	private String password;
	private String email;
	private String phone;
	private String sex;
	private Integer clazzId;
	private String img;
	private String pwdRepaire;
	private Integer type;
	private String disname;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Integer getClazzId() {
		return clazzId;
	}
	public void setClazzId(Integer clazzId) {
		this.clazzId = clazzId;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getPwdRepaire() {
		return pwdRepaire;
	}
	public void setPwdRepaire(String pwdRepaire) {
		this.pwdRepaire = pwdRepaire;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getDisname() {
		return disname;
	}
	public void setDisname(String disname) {
		this.disname = disname;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", email=" + email + ", phone=" + phone
				+ ", sex=" + sex + ", classId=" + clazzId + ", img=" + img + ", pwdRepaire=" + pwdRepaire + ", type="
				+ type + ", disname=" + disname + "]";
	}


	
	
}
